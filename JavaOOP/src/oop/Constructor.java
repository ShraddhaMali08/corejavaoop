package oop;

public class Constructor {
	
	int a;
	public Constructor() {				//default constructor
		 a=5;
		
	}
	
	public Constructor(int b) {			//parameterized constructor
		b=10;
		
		a=b;
	}
	
	public static void main(String args[]) {
		Constructor obj1=new Constructor();
		Constructor obj2=new Constructor(10);
		System.out.println(obj1.a);
		System.out.println(obj2.a);
		
		
	}

}
