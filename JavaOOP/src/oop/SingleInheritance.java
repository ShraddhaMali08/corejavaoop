package oop;


class Base{
	 int a = 10;
	    void Display() {
	        System.out.println("a = "+a);
	        System.out.println("Base class");
	    }
}
public class SingleInheritance extends Base{
	public static void main(String[] args) {

		int b=20;
		System.out.println("derived class");
		System.out.println("b ="+b);
		SingleInheritance obj =new SingleInheritance();
		obj.Display();
	
		
		
	}
	
}
